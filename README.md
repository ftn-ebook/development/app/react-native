**Project**
- started by: David Chung
- Managed by: David Chung, Sawyer Kim, Chuyeon Lee
- Contributors: Sakan Pengdis, Siu, Yuval Dolev, Tony, Eric Choi, Daniel Song, Hemosoo, Samuel, Hannah Moon, Jaden, Julia Jung, Solomon, and Yunju Song

**Team**
- UI and UX planning: Siu, Eric Choi, Yunju Song, Julia Jung, and Hannah Moon
- Frontend: Sakan Pengdis, Siu, Yuval Dolev, Eric Choi, Yunju Song, Solomon
- Server Integrations: Yuval Dolev, Hemosoo, Jaden, Samuel, Eric Choi
- Crawling: Tony, Daniel Song, Tony, Julia Jung, Hannah Moon


**dependencies used:**
npm install 
1. @react-navigation/native 
2. @react-navigation/stack
3. @react-navigation/bottom-tabs
4. npm install react-native-reanimated react-native-gesture-handler react-native-screens react-native-safe-area-context @react-native-community/masked-view
5. react-native-elements
6. @react-native-community/picker

This video is really helpful: https://www.youtube.com/watch?v=nQVCkqvU1uE


**Screenshot**
https://gitlab.com/ftn-ebook/development/app/react-native/-/issues/1

